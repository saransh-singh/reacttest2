'use strict';
/***************************************************************************************************************************

 * Shared Service use by diffent controllers 
 
 **************************************************************************************************************************/
var path = require('path');
var _ = require('lodash');
var NETWORK_SERVICE = require('./network');
var FILE = require('./fileSystem');
// All Service will extend these Service
// ============================================
var COMMON_SERVICES = {
    //get index of object in array (ES6)
    getObjectIndex: function(array, key, value) {
        return array.findIndex(ind => ind[key] == value);
    },

    // Get Count of doc present in mongoDB
    _getDocCount: function(collection, where, cb) {
        if (!cb && typeof cb != 'function') return "Invaild callback";
        if (!collection) return cb("Provide collection", null);
        if (!where && typeof where != 'object') return cb("Invaild query", null);
        collection.find(where).count().exec(function(err, count) {
            if (!err && count > 0) {
                return cb(err, count)
            } else {
                return cb(err, 0);
            }
        });
    },
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
    COMMON_SERVICES,
    NETWORK_SERVICE,
    FILE
);