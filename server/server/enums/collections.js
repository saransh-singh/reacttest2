(function() {
    'use strict';
    //* List of all collection of this application
    //* Don't change id
    //* Change collection name as per your system
    module.exports = {
        TODO_APP_DB_COLLECTIONS: {
            TRANSACTION_TYPE: {
                id: 1,
                name: 'transactionType'
            },
        }
    }
}());