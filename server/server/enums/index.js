'use strict';

var _ = require('lodash');
var collection = require('./collections');
/***************************************************************************************************************************
 Export all enums
 **************************************************************************************************************************/

module.exports = _.merge(
    collection
);