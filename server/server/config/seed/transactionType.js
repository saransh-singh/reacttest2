/**
 * Code to Populate DB with dummy data on starting the server
 */

'use strict';

var chalk = require('chalk');

function start(req, res, next) {
    // Require TransactionType Collection 
    var TransactionType = require('../../api/transactionType/transactionType.model');

    TransactionType.find(function(err, data) {
        if (err)
            console.log(chalk.red('Hey! %s Error occured while Populating TransactionTypes'), process.env.USER);
        if (data.length < 1) {
            TransactionType.create({
                name: 'money',
                adapterConfig: {
                    type: 'database',
                    database: 'mongo',
                    dbName: 'money',
                    connectionURL: 'mongodb://localhost/money',
                    collectionName: 'transaction'
                },
                fields: [{
                    name: 'to',
                    type: 'input',
                    field: 'text',
                }, {
                    name: 'from',
                    type: 'input',
                    field: 'text',
                }, {
                    name: 'money',
                    type: 'input',
                    field: 'number',
                }]
            }, {
                name: 'file',
                adapterConfig: {
                    type: 'database',
                    database: 'mongo',
                    dbName: 'file',
                    connectionURL: 'mongodb://localhost/file',
                    collectionName: 'transaction'
                },
                fields: [{
                    name: 'to',
                    type: 'input',
                    field: 'text',
                }, {
                    name: 'from',
                    type: 'input',
                    field: 'text',
                }, {
                    name: 'file',
                    type: 'input',
                    field: 'file',
                }]
            }, function(clientError, TransactionTypes) {
                if (clientError) {
                    console.log(chalk.red('Hey! %s Error occured while populating TransactionTypes'), process.env.USER);
                } else {
                    console.log(chalk.greenBright('Hey! %s transaction type has been populated in database'), process.env.USER);
                }
            });
        } else {
            console.log(chalk.greenBright('Hey! %s transaction type has been already there in database..'), process.env.USER);
        }

    });

}

exports.start = start;