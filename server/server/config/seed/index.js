/**
 * Code to Populate DB with dummy data on starting the server
 */

'use strict';

var transactionType = require('./transactionType');

function start() {
    transactionType.start();
}

exports.start = start;