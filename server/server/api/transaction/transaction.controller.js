'use strict';

var Services = require('../../service');
var TransactionType = require('../transactionType/transactionType.model');

/***************************************************************************************************************************
 * list transactions based on transaction type
 * API /api/transaction/:transTypeID
 * METHOD GET 
 * Secuirty -> Open
 **************************************************************************************************************************/
exports.index = async (req, res) => {
  if(!req.params.transTypeID) {
    return  Services._handleError(res, 'Required Field Is Missing');
  }

  const transType = await TransactionType.findById(req.params.transTypeID, 'adapterConfig');

  if(!transType) {
    return  Services._handleError(res);
  }


  let mongoose = require('mongoose');

  let connnection = mongoose.createConnection(transType.adapterConfig.connectionURL);
  let TransModel = connnection.model(transType.adapterConfig.collectionName, new mongoose.Schema({}), transType.adapterConfig.collectionName);
  let transactions = await TransModel.find().populate({
                        path: 'transactionType',
                        select: 'name',
                        model: TransactionType
                    });
  return Services._response(res, transactions);
};

/***************************************************************************************************************************
 * Create transactions based on transaction type
 * API /api/transaction/
 * METHOD POST 
 * Secuirty -> Open
 **************************************************************************************************************************/
exports.post = async (req, res) => {

  try{
    if(!req.body.transType) {
      return  Services._handleError(res, 'Required Field Is Missing');
    }

    const transType = await TransactionType.findById(req.body.transType, 'adapterConfig name');
    
    if(!transType) {
        return  Services._handleError(res);
    }

    let mongoose = require('mongoose');
    let connnection = mongoose.createConnection(transType.adapterConfig.connectionURL);


    let transSchema = new mongoose.Schema({}, { strict: false });
    let TransModel = connnection.model(transType.adapterConfig.collectionName, transSchema, transType.adapterConfig.collectionName);

    let transaction;
    if(req.body.base64) {
        const docName = `${Math.floor(100000 + Math.random() * 900000)}.${req.body.base64.extension}`
        const uploadLoc = `/client/assets/file/${docName}`;
        const file = `/assets/file/${docName}`;
        // create file url from server directory
        const url = `${process.env["PWD"]}${uploadLoc}`;

        const documentFile = { url, fileData: req.body.base64.fileData };
        // upload file on server
        Services.upload(documentFile, ()=>{});
        transaction = new TransModel({
          to: req.body.to,
          from: req.body.from,
          transactionType: req.body.transType,
          file,
        })
      } else {
        transaction = new TransModel({
          to: req.body.to,
          from: req.body.from,
          transactionType: req.body.transType,
          money: req.body.money
        });
      }

    if(transaction === null) return Services._handleError(res);

    transaction
    .save()
    .then(result => {
      return Services._response(res, result);
    })
    .catch(err => {
      return Services._handleError(req, res, err);
    });


  } catch(e) {
    return Services._handleError(res, e);
  }
}