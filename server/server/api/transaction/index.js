'use strict';

var express = require('express');
var controller = require('./transaction.controller');
var router = express.Router();

//Sub routes of transaction module
router.get('/:transTypeID', controller.index);
router.post('/', controller.post);

module.exports = router;