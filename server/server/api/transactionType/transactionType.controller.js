'use strict';

var Services = require('../../service');
var TransactionType = require('./transactionType.model');
/***************************************************************************************************************************
 * list all the transactions types in system
 * API /api/transactionType/
 * METHOD GET 
 * Secuirty -> Open
 **************************************************************************************************************************/
exports.index = function(req, res) {
    TransactionType.find()
    .select({ adapterConfig:0 })
    .then(transactionTypes => {
      return Services._response(res, transactionTypes);
    })
    .catch(err => {
      console.log(err);
      return Services._handleError(req, res, err);
    });
};
