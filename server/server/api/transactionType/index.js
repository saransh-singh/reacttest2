'use strict';

var express = require('express');
var controller = require('./transactionType.controller');
var router = express.Router();

//Sub routes of transaction type module
router.get('/', controller.index);

module.exports = router;