/**
 * Main application routes
 */

'use strict';

var path = require('path');

module.exports = function(app) {

    // Insert routes below
     app.use('/api/transaction', require('./api/transaction'));
     app.use('/api/transactionType', require('./api/transactionType'));

    // All other routes should redirect to the index.html
    app.route('/*')
        .get(function(req, res) {
            if (process.env.NODE_ENV == "development")
                res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
            if (process.env.NODE_ENV == "production")
                res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
        });
};