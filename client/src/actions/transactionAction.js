

import { SET_TRANSACTION_DETAILS, SET_TRANSACTION_TYPE } from './types'
import axios from 'axios';
export const transactionType = () => dispatch => {
 axios.get('http://127.0.0.1:7000/api/transactionType/')
  .then(function (response) {
   // handle success
   dispatch({ type: SET_TRANSACTION_TYPE, payload: response.data.data });
  })
  .catch(function (error) {
   // handle error
   console.log(error);
  })
};
export const addTransaction = (data) => dispatch => {

 axios.post(`http://127.0.0.1:7000/api/transaction`,data)
  .then(function (response) {
 

  })
  .catch(function (error) {
   // handle error
   console.log(error);
  })

}
export const transactionDetails = (id) => dispatch => {
 axios.get(`http://127.0.0.1:7000/api/transaction/${id}`)
  .then(function (response) {
   // handle success
   dispatch({ type: SET_TRANSACTION_DETAILS, payload: response.data.data });
  })
  .catch(function (error) {
   // handle error
   console.log(error);
  })


};