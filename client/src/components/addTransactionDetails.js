import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem'
import FileBase64 from 'react-file-base64';
import InputLabel from '@material-ui/core/InputLabel';
import { addTransaction } from '../actions/transactionAction'
import { connect } from 'react-redux'
const selectStyle = {
  color: "red",
  width: "100%",
  height: "30px",
  marginTop: "20px"
};
const buttonStyle = {
  width: "100%",
  marginTop: "50px"
}
const label = {
  marginTop: "20px",
  display: "inline-block"
}
class AddTransaction extends Component {

  constructor(props) {
    super(props)
    this.state = {
      to: '',
      from: '',
      transType: 1,
      file: '',
      money: '',
      base64: null,
    }
  }
  
  onChangeHandler = (e) => {
    this.setState({ [e.target.name]: e.target.value })
  }
  onChangeType = e => {
    this.setState({ transType: e.target.value });
  };
  getBaseFile1 = files => {
    this.setState({ file: files.base64 });
  };



  onUploadImage = (event) => {
    const file = event.target.files[0];
    const reader = new window.FileReader();
    const extension = file.name.split(".").pop();
    const imageArr = ["pdf"];

    if (file) {
        if (imageArr.indexOf(extension.toLowerCase()) === -1) {
            alert('Invalid image file extension');
            //event.currentTarget.value = "";
            event.target.value = "";
            return false;
        }
        reader.onload = fileLoadEvent => {
            this.setState({
              base64: {
                extension: extension,
                fileData: reader.result
              }
            });
        };
        reader.readAsBinaryString(event.currentTarget.files[0]);
    }
  }
  submit = (e) => {
   
    

    e.preventDefault();
    const userData = {
      to: this.state.to,
      from: this.state.from,
      transType: this.state.transType,
      base64: this.state.base64,
    };
    if(this.state.transType==1)
      userData.money = this.state.money;
    else
      userData.file = this.state.file;
    this.props.addTransaction(userData)
    window.location.reload();
  }
  render() {
    console.log("ADWDSADASDSAFSFSDF",this.state.value)
    return (
      <>
        <TextField

          name="to"
          className=""
          id="standard-name"
          label="Source"
          margin="normal"
          onChange={this.onChangeHandler}

        />
        <br />
        <TextField
          name="from"
          id="standard-name"
          label="Destination"
          margin="normal"
          onChange={this.onChangeHandler}
        />
        <br />
        <label style={label}>Transaction Type</label><br />
        <select onChange={this.onChangeType} style={selectStyle}>
          <option value={this.props.transactionTypeList.transactionType[0] && this.props.transactionTypeList.transactionType[0]._id} >
            {this.props.transactionTypeList.transactionType[0] && this.props.transactionTypeList.transactionType[0].name}
          </option>

          <option value={this.props.transactionTypeList.transactionType[1] && this.props.transactionTypeList.transactionType[1]._id} 
          >{this.props.transactionTypeList.transactionType[1] && this.props.transactionTypeList.transactionType[1].name}</option>
        </select>
        <br />
        {
          this.state.transType == 1 ?
            <TextField
              name="money"
              className=""
              id="standard-name"
              label="Amount"
              margin="normal"
              onChange={this.onChangeHandler}

            />
            :
            <label className="file-upload">
            <input type="file" onChange={(e) => this.onUploadImage(e)} />
              {/* <img src={baseImage} ></img> */}
            </label>}
        <br />
        <Button variant="contained" color="primary" onClick={this.submit} style={buttonStyle}>
          Submit
      </Button>

      </>
    );
  }
}
const mapStateToProps = state => ({
  // auth: state.auth,
  // isLoading:state.isLoading
  transactionTypeList: state.transactionType,
  transactionDetailsData:state.transactionDetails

});
export default connect(mapStateToProps, { addTransaction })(AddTransaction);