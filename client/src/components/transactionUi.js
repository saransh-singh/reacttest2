import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow);
const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
    boxShadow: "none"
  },
  table: {
    width: "1000px",
    left: "0",
    right: "0",
    margin: "0 auto",
    boxShadow: "none"
  },
}));
export default function (props) {
  console.log("props aresdsadasdsad",props)
  const classes = useStyles();
  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <StyledTableCell>Transaction ID</StyledTableCell>
            <StyledTableCell align="right">Source</StyledTableCell>
            <StyledTableCell align="right">Destination</StyledTableCell>
            {
              props.id==1?
            <StyledTableCell align="right">Amount</StyledTableCell>         
            :<StyledTableCell align="right">File</StyledTableCell> }
            </TableRow>
        </TableHead>
        <TableBody>
          {props.rows.map(row => (
            <StyledTableRow key={row.value}>
              <StyledTableCell component="th" scope="row">
                {row._id}
              </StyledTableCell>
              <StyledTableCell align="right">{row.to}</StyledTableCell>
              <StyledTableCell align="right">{row.from}</StyledTableCell>
              {
                row.money?
              <StyledTableCell align="right">{row.money}</StyledTableCell>
             : 
             <StyledTableCell align="right">

              {row.file}
            </StyledTableCell>}
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
}