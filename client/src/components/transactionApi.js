import React, { Component } from 'react';
import TransactionUi from './transactionUi'
import Button from '@material-ui/core/Button';
import Modal from 'react-responsive-modal';
import AddTransaction from './addTransactionDetails'
import { connect } from 'react-redux'
import { transactionType } from '../actions/transactionAction'
import { transactionDetails } from '../actions/transactionAction'

const rows = [
  { transactionId: 1, to: "Brad", from: "Joe", amount: 500 },
  { transactionId: 2, to: "Brad", from: "Joe", amount: 500 },
  { transactionId: 3, to: "Brad", from: "Joe", amount: 500 },
  { transactionId: 4, to: "Brad", from: "Joe", amount: 500 },
];
const button = {
  marginTop: "30px"
}
const selectStyle = {
  
  width: "50%",
  height: "30px",
  marginTop: "20px"
};
class TransactionApi extends Component {

  constructor(props) {
    super(props)
    this.state = {
     
      setOpen: false,
      
      id:'1'
    }
  }
  componentDidMount() {

    this.props.transactionType()
    this.props.transactionDetails(this.state.id)
   
  }

  onChangeType = (e) => {
    
    // this.setState({ value: e.target.value });
    this.setState({id:e.target.value})
    this.props.transactionDetails(e.target.value)
  };
  handleOpen = () => {
    this.setState({ setOpen: true })

  };

  handleClose = () => {
    this.setState({ setOpen: false })
  };
  render() {
    console.log("propasadasdasds", this.state.id)
    return (
      <>
      
        <select onChange={this.onChangeType} style={selectStyle}>
          <option value={this.props.transactionTypeList.transactionType[0] && this.props.transactionTypeList.transactionType[0]._id} >
            {this.props.transactionTypeList.transactionType[0] && this.props.transactionTypeList.transactionType[0].name}
          </option>

          <option value={this.props.transactionTypeList.transactionType[1] && this.props.transactionTypeList.transactionType[1]._id} 
          >{this.props.transactionTypeList.transactionType[1] && this.props.transactionTypeList.transactionType[1].name}</option>
        </select>
        {
          this.props.transactionDetailsData.transactionDetails && (<TransactionUi rows={this.props.transactionDetailsData.transactionDetails} id={this.state.id}/>)
        }


        <Button variant="contained" color="primary" onClick={this.handleOpen} style={button}>
          Add
      </Button>
        <Modal open={this.state.setOpen} onClose={this.handleClose} center>
          <AddTransaction />
        </Modal>
      </>
    );
  }
}
const mapStateToProps = state => ({
  // auth: state.auth,
  // isLoading:state.isLoading
  transactionTypeList: state.transactionType,
  transactionDetailsData:state.transactionDetails

});

export default connect(mapStateToProps, { transactionType, transactionDetails })(TransactionApi);
