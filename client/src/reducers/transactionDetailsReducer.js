import {SET_TRANSACTION_DETAILS} from '../actions/types'

let initialstate={
 
  transactionDetails:[]
}
export default function(state=initialstate,action){
  switch(action.type){
    case SET_TRANSACTION_DETAILS :
    return{
      ...state,
      transactionDetails:action.payload
    }
    default :
        return  state
  }
}