import { combineReducers } from 'redux';
//import authReducer from './authReducer';
//import userProfileReducer from './userProfileReducer'
import trasactionTypeReducer from './transactionTypeReducer'
import trasactionDetailsReducer from './transactionDetailsReducer'


//import errorReducer from './errorReducer';

export default combineReducers({
  // auth: authReducer,
  // userProfile:userProfileReducer
 // errors: errorReducer
 transactionType:trasactionTypeReducer,
 transactionDetails:trasactionDetailsReducer
});
