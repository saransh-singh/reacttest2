import {SET_TRANSACTION_DETAILS,SET_TRANSACTION_TYPE} from '../actions/types'

let initialstate={
  transactionType:[],
 
}
export default function(state=initialstate,action){
  switch(action.type){
   
    case SET_TRANSACTION_TYPE :
    return{
      ...state,
      transactionType:action.payload
    }
    default :
        return  state
  }
}